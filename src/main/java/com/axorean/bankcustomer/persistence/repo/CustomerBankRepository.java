package com.axorean.bankcustomer.persistence.repo;

import com.axorean.bankcustomer.persistence.entity.CustomerBank;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerBankRepository extends JpaRepository<CustomerBank, Integer> {
    CustomerBank findCustomerBankByEmail(String email);
}

