package com.axorean.bankcustomer.persistence.dao.impl;

import com.axorean.bankcustomer.persistence.dao.CustomerBankDao;
import com.axorean.bankcustomer.persistence.entity.CustomerBank;
import com.axorean.bankcustomer.persistence.repo.CustomerBankRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class CustomerBankDaoImpl implements CustomerBankDao {
    private final CustomerBankRepository customerBankRepository;
    @Override
    public CustomerBank findCustomerBankByEmail(String email) {
        return customerBankRepository.findCustomerBankByEmail(email);
    }
}
