package com.axorean.bankcustomer.persistence.dao;

import com.axorean.bankcustomer.persistence.entity.CustomerBank;

public interface CustomerBankDao {
    CustomerBank findCustomerBankByEmail(String email);
}
