package com.axorean.bankcustomer.service;

import com.axorean.bankcustomer.persistence.dao.CustomerBankDao;
import com.axorean.bankcustomer.persistence.entity.CustomerBank;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CustomerBankService {
    private final CustomerBankDao customerBankDao;

    public boolean findCustomerBank(String email) {
        return this.customerBankDao.findCustomerBankByEmail(email);
    }
}
